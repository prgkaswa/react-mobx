import React, { Component } from 'react';
import Product from './components/Product';
import { Provider } from 'mobx-react';
import { withRouter } from 'react-router';
import { Route } from 'react-router-dom';
import Store from './Store';
import { decorate, observable, action } from 'mobx';
import './App.css';
import AddProduct from "./components/AddProduct";
import EditProduct from "./components/EditProduct";
import DetailProduct from "./components/DetailProduct";

decorate(Store, {
  products: observable,
  submitDataProduct: action
});
const shoppingStore = new Store();
class App extends Component {
  render() {
    return (
      <Provider store={shoppingStore}>
        {/* <Auth /> */}
        <div className='container'>
          <Route
            exact
            path='/'
            render={() => (
              <Product
                history={this.props.history}
              />
            )}
          />
          <Route
            exact
            path='/add'
            render={() => <AddProduct history={this.props.history} />}
          />
          <Route
            exact
            path="/edit/:id"
            render={() => <EditProduct />}
          />
          <Route
            exact
            path="/detail/:id"
            render={() => <DetailProduct/>}
          />
        </div>
      </Provider>
    );
  }
}
export default withRouter(App);