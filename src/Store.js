import axios from 'axios';

class Store {
    id = 0
    products = [];
    carts = [];
    currentCart = [];
    newProduct = {
      name: '',
      sku: 0,
      price: 0,
      description: ''
    }
    removeFromCart(id) {
      this.carts = this.carts.filter(item => {
        return item.product_id !== id;
      });
      this.getCart();
    }
    increaseQuantityInCart(id) {
      this.carts.map(item => {
        if (item.product_id === id) item.quantity += 1;
        return item;
      });
      this.getCart();
    }
    decreaseQuantityInCart(id) {
      this.carts.map(item => {
        if (item.product_id === id && item.quantity > 1) item.quantity -= 1;
        return item;
      });
      this.getCart();
    }
    addToCart(id) {
      let found = false;
      this.carts.map(item => {
        if (item.product_id === id) {
          item.quantity += 1;
          found = true;
        }
        return item;
      });
      if (!found) {
        this.carts.push({ product_id: id, quantity: 1 });
      }
      this.getCart();
    }
    getCart() {
      let carts = this.carts;
      carts.map(item => {
        for (let i in this.products) {
          if (item.product_id === this.products[i].id) {
            item.image = this.products[i].image;
            item.name = this.products[i].name;
            item.description = this.products[i].description;
            item.price = this.products[i].price * item.quantity;
          }
        }
        return item;
      });
      this.currentCart = carts;
    }
    loading = true;
    auth0 = null;
    authenticated = null;
    setLoader(loading) {
      this.loading = loading;
    }
    setAuth(token) {
      this.authenticated = token;
    }
    initialize(auth0) {
      this.auth0 = auth0;
    }
    addProduct(val, idx) {
      this.newProduct[idx] = val.target.value
    }
    async submitDataProduct(type) {
      let data = this.newProduct
      await axios.post(`http://localhost:8085/api/product`, data);
    }
    async editDataProduct(id) {
      let data = this.newProduct
      delete data.id // delete property id untuk menyesuaikan request data
      delete data.url_image // delete property image untuk menyesuaikan request data
      await axios.put(`http://localhost:8085/api/product/` + id, data);
    }
    async deleteDataProduct(id) {
      await axios.delete(`http://localhost:8085/api/product/` + id);
    }
    async listProduct() {
      let res = await axios.get(`http://localhost:8085/api/list`);
      this.products = res.data
      return res.data
    }
    async getProductById(id) {
      let data = await axios.get(`http://localhost:8085/api/product/` + id);
      this.newProduct = data.data
      return data.data
    }

    async update() {
      let data = this.newProduct
      await axios.post(`http://localhost:8085/api/product`, data);
    }
    changeNewProduct(data) {
      this.newProduct = data
    }
    setId (id) {
      this.id = id
    }
}
export default Store;