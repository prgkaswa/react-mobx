import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import axios from 'axios';
import Cart from './Cart';
@inject('store')
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  async componentWillMount() {
    if (!this.props.store.authenticated) {
      this.props.history.push("/")
    }
    let listData = await this.props.store.listProduct()
    this.setState({products: listData})
  }
  addToCart(id) {
    this.props.store.addToCart(id);
  }

  async deleteProduct(id) {
    await this.props.store.deleteDataProduct(id)
    window.location.replace("/");
    alert('Data has been deleted')
  }

  updateProduct(id) {
    this.props.store.updateProduct(id);
  }

  list(data, index) {
    return (
      <div key={index} className='col-md-4 top-space'>
        <div className='card'>
          <button onClick={() => this.props.history.push("/detail/" + data.id)}>
            <img
            className='card-img-top'
            height={200}
            src={data.url_image}
            alt='Product stuff'
          />
          </button>
          <div className='card-body'>
            <h4 className='card-title'>{data.name}</h4>
            <p className='card-text'>{data.description}</p>
            <p className='card-text'>Price    : {data.price.toFixed(2)}</p>     
            <p className='card-text'>Stock in : {data.sku}</p>            
            <div className='detail'>
              <button onClick={() => this.props.history.push("/edit/" + data.id)} className='btn btn-primary red btn-sm'>Edit</button>
              <button onClick={() => this.deleteProduct(data.id)} className='btn btn-danger btn-sm'>Delete</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
  render() {
    return (
      <div className='row'>
        <div className='col-md-8'><h4>Dashboard</h4></div>
        <div className='col-md-8  ' >
          <button onClick={() => this.props.history.push("/add")} className='btn btn-success btn-sm'>Add Product</button>
        </div>
        <div className='col-md-8'>
          <div className='row'>
            {this.state.products && this.state.products.length > 0 ? this.state.products.map((data, index) =>
              this.list(data, index)
            ) : <div align="center"><br></br><br/><p>No Data</p></div>}
          </div>
        </div>
        <div className='col-md-4'>
          <div className='top-space'>
            <Cart />
          </div>
        </div>
      </div>
    );
  }
}
export default observer(Product);