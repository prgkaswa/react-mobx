import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';

@inject('store')
@observer
@withRouter

class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            sku: 0,
            price: 0,
            description: ''
        };
    }

    async componentWillMount() {
        let id = this.props.match.params.id;
        let getdata = await this.props.store.getProductById(id)
        this.setState({name: getdata.name, sku: getdata.sku, price: getdata.price, description: getdata.description, url_image: getdata.url_image})
    }
    
    render() {
    return (
        <div>
            <div>
            <button onClick={() => this.props.history.push("/")} className='btn btn-info btn-sm'>Back Dashboard</button>
            </div>
            
            <div key={"detail"} className='col-md-4 top-space'>
                <h3>Product Detail</h3>
                <div className='card'>
                <img
                    className='card-img-top'
                    height={200}
                    src={this.state.url_image}
                    alt='Product stuff'
                />
                <div className='card-body'>
                    <h4 className='card-title'>{}</h4>
                    <p className='card-text'>{this.state.description}</p>
                    <p className='card-text'>Price    : {this.state.price.toFixed(2)}</p>     
                    <p className='card-text'>Stock : {this.state.sku}</p>
                </div>
                </div>
            </div>
        </div>
        );
    }
}
export default observer(Detail);