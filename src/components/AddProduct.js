import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
@inject('store')
@observer
class AddProduct extends Component {
  async submitData() {
    await this.props.store.submitDataProduct('add')
    window.location.replace("/");
    alert("Data Success Submit.")
  }
  render() {
    return (
      <div>
        <div>
          <button onClick={() => this.props.history.push("/")} className='btn btn-info btn-sm'>Back Dashboard</button>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-25">
              <label for="pdname">Product Name</label>
            </div>
            <div class="col-75">
              <input type="text" id="pname" name="productname" placeholder="Ex. Samsung A20" onChange={(e) => this.props.store.addProduct(e, 'name')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="skuname">SKU</label>
            </div>
            <div class="col-75">
              <input type="text" id="skuname" name="sku" placeholder="Ex. 100" onChange={(e) => this.props.store.addProduct(e, 'sku')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="price">Product Price</label>
            </div>
            <div class="col-75">
              <input type="text" id="price" name="price" placeholder="Ex. 20000000" onChange={(e) => this.props.store.addProduct(e, 'price')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="description">Description</label>
            </div>
            <div class="col-75">
              <textarea id="description" name="description" placeholder="Write something.." onChange={(e) => this.props.store.addProduct(e, 'description')}></textarea>
            </div>
          </div>
          <div class="row">
            <input type="submit" value="Submit" onClick={(e) => this.submitData()}/>
          </div>
        </div>
      </div>
    );
  }
}
export default AddProduct;