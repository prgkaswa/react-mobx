import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import axios from 'axios';
import { withRouter } from 'react-router';


@inject('store')
@observer
@withRouter

class EditProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      sku: 0,
      price: 0,
      description: ''
    };
  }

  async componentWillMount(){
    let id = this.props.match.params.id;
    let getdata = await this.props.store.getProductById(id)
    this.setState({name: getdata.name, sku: getdata.sku, price: getdata.price, description: getdata.description})
  }

  onChange(val, idx) {
    this.props.store.addProduct(val, idx)
    let datastate = {}
    datastate[idx] = val.target.value
    this.setState(datastate)
  }

  async onSubmit(id) {
    await this.props.store.editDataProduct(id)
    // await window.location.replace("/");
    alert("Data Success Updated.")
  }

  render() {
    return (
      <div>
        <div>
          <button onClick={() => this.props.history.push("/")} className='btn btn-info btn-sm'>Back Dashboard</button>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-25">
              <label for="pdname">Product Name</label>
            </div>
            <div class="col-75">
              <input type="text" id="pname" name="productname" value={this.state.name} placeholder="Ex. Samsung A20" onChange={(e) => this.onChange(e, 'name')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="skuname">SKU</label>
            </div>
            <div class="col-75">
              <input type="text" id="skuname" name="sku" placeholder="Ex. 100" value={this.state.sku} onChange={(e) => this.onChange(e, 'sku')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="price">Product Price</label>
            </div>
            <div class="col-75">
              <input type="text" id="price" name="price" placeholder="Ex. 20000000" value={this.state.price} onChange={(e) => this.onChange(e, 'price')}/>
            </div>
          </div>
          <div class="row">
            <div class="col-25">
              <label for="description">Description</label>
            </div>
            <div class="col-75">
              <textarea id="description" name="description" placeholder="Write something.." value={this.state.description} onChange={(e) => this.onChange(e, 'description')}></textarea>
            </div>
          </div>
          <div class="row">
            <input type="submit" value="Submit" onClick={(e) => this.onSubmit(this.props.match.params.id)}/>
          </div>
        </div>
      </div>
    );
  }
}
export default EditProduct;